package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment               string
	PostgresHost              string
	PostgresPort              string
	PostgresUser              string
	PostgresPassword          string
	PostgresDatabase          string
	LogLevel                  string
	PGXPoolMax                int
	TaskServiceHost           string
	TaskServicePort           string
	RabbitMQHost              string
	RabbitMQPort              string
	RabbitMQUser              string
	RabbitMQPassword          string
	RabbitMQConnectionTry     int
	RabbitMQConnectionTimeOut int
	RemainderTopic            string
	RemainderType             string
	MissRemainderType         string
	UserServiceHost           string
	UserServicePort           string
}

func LoadConfig() *Config {
	c := &Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop")) // develop, staging, production
	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))
	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToString(GetOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "task_service"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))
	c.PGXPoolMax = cast.ToInt(GetOrReturnDefault("PGX_POOL_MAX", 2))

	c.RabbitMQConnectionTimeOut = cast.ToInt(GetOrReturnDefault("RABBIT_MQ_CONNECTION_TIME_OUT", 5))
	c.RabbitMQConnectionTry = cast.ToInt(GetOrReturnDefault("RABBIT_MQ_CONNECTION_TRY", 100))
	c.RabbitMQHost = cast.ToString(GetOrReturnDefault("RABBIT_MQ_HOST", "localhost"))
	c.RabbitMQPort = cast.ToString(GetOrReturnDefault("RABBIT_MQ_PORT", "5672"))
	c.RabbitMQUser = cast.ToString(GetOrReturnDefault("RABBIT_MQ_USER", "azizbek"))
	c.RabbitMQPassword = cast.ToString(GetOrReturnDefault("RABBIT_MQ_PASSWORD", "azizbek"))
	c.RemainderTopic = cast.ToString(GetOrReturnDefault("REMAINDER_TOPIC", "remind"))
	c.RemainderType = cast.ToString(GetOrReturnDefault("REMAINDER_TYPE", "remind"))
	c.MissRemainderType = cast.ToString(GetOrReturnDefault("MISS_REMAINDER_TYPE", "miss_remind"))

	c.TaskServicePort = cast.ToString(GetOrReturnDefault("TASK_SERVICE_PORT", "9998"))
	c.TaskServiceHost = cast.ToString(GetOrReturnDefault("TASK_SERVICE_HOST", "localhost"))
	c.UserServiceHost = cast.ToString(GetOrReturnDefault("USER_SERVICE_HOST", "localhost"))
	c.UserServicePort = cast.ToString(GetOrReturnDefault("USER_SERVICE_PORT", "9999"))

	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
