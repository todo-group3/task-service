package main

import (
	"gitlab.com/todo-group/task-service/config"
	"gitlab.com/todo-group/task-service/internal/app"
	cronjob "gitlab.com/todo-group/task-service/internal/cron_job"
)

func main() {
	// Configuration
	cfg := config.LoadConfig()

	// Run Cron job in goroutine
	go cronjob.Run(cfg)
	
	// Run
	app.Run(cfg)

}
