CREATE TABLE IF NOT EXISTS tasks (
    id uuid PRIMARY KEY,
    user_id uuid NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    start_time TIMESTAMP NOT NULL,
    finish_time TIMESTAMP NOT NULL,
    done_time TIMESTAMP,
    reminded_to_start TIMESTAMP,
    reminded_to_finish TIMESTAMP,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP
);