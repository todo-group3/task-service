FROM golang:1.19.3-alpine
RUN mkdir task
COPY . /task
WORKDIR /task
RUN go build -o main cmd/app/main.go
CMD ./main
EXPOSE 9998