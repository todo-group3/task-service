run:
	go run cmd/app/main.go

proto-gen:
	./script/gen-proto.sh
	
add-submodule:
	git submodule add git@gitlab.com:todo-group3/protos.git

update-submodule:
	git submodule update --remote --merge

migrate_up:
	migrate -path migrations/ -database postgres://todo:todopassword@adds-uz.cgjaagroetzx.ap-northeast-1.rds.amazonaws.com:5432/todo_task up

migrate_down:
	migrate -path migrations/ -database postgres://todo:todopassword@adds-uz.cgjaagroetzx.ap-northeast-1.rds.amazonaws.com:5432/todo_task down
