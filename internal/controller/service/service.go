package service

import (
	grpcclient "gitlab.com/todo-group/task-service/internal/controller/service/grpcClient"
	"gitlab.com/todo-group/task-service/internal/controller/storage"
	"gitlab.com/todo-group/task-service/internal/rabbitmq"
	"gitlab.com/todo-group/task-service/pkg/db"
	"gitlab.com/todo-group/task-service/pkg/logger"
)

type TaskService struct {
	Logger   *logger.Logger
	Clients  grpcclient.Clients
	Storage  storage.IStorage
	RabbitMq *rabbitmq.RabbitMQ
}

func NewTaskService(l *logger.Logger, client grpcclient.Clients, stg *db.Postgres, rmq *rabbitmq.RabbitMQ) *TaskService {
	return &TaskService{
		Logger:   l,
		Clients:  client,
		Storage:  storage.NewStoragePg(stg),
		RabbitMq: rmq,
	}
}
