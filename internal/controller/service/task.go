package service

import (
	"context"
	"fmt"

	"gitlab.com/todo-group/task-service/genproto/task"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (t *TaskService) ScheduleTask(ctx context.Context, req *task.ScheduleReq) (*task.ScheduleRes, error) {
	res, err := t.Storage.Task().ScheduleTask(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.ScheduleTask - : %w", err))
		return &task.ScheduleRes{}, status.Error(codes.Internal, "Couldn't schedule task")
	}

	return res, nil
}

func (t *TaskService) UpdateTask(ctx context.Context, req *task.ScheduleReq) (*task.ScheduleRes, error) {
	res, err := t.Storage.Task().UpdateTask(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.UpdateTaske - : %w", err))
		return &task.ScheduleRes{}, status.Error(codes.Internal, "Couldn't update task")
	}

	return res, nil
}

func (t *TaskService) GetAllTasks(ctx context.Context, req *task.OneFieldReq) (*task.GetAllTasksRes, error) {
	res, err := t.Storage.Task().GetAllTasks(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.GetAllTasks - : %w", err))
		return &task.GetAllTasksRes{}, status.Error(codes.Internal, "Couldn't get all of your tasks  ")
	}

	return res, nil
}

func (t *TaskService) DoneTask(ctx context.Context, req *task.OneFieldReq) (*task.Empty, error) {
	if ok, _ := t.Storage.Task().IsDone(req.Value); ok {
		return &task.Empty{}, status.Error(codes.InvalidArgument, "This task has already been done")
	}
	res, err := t.Storage.Task().DoneTask(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.DoneTask - : %w", err))
		return &task.Empty{}, status.Error(codes.Internal, "Couldn't make it done task")
	}

	return res, nil
}

func (t *TaskService) DeleteTask(ctx context.Context, req *task.OneFieldReq) (*task.Empty, error) {
	res, err := t.Storage.Task().DeleteTask(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.DeleteTask - : %w", err))
		return &task.Empty{}, status.Error(codes.Internal, "Couldn't delete task")
	}

	return res, nil
}

func (t *TaskService) GetTasksToBeReminded(ctx context.Context, req *task.GetTasksToBeRemindedReq) (*task.TasksToBeReminded, error) {
	res, err := t.Storage.Task().GetTasksToBeReminded(req)
	if err != nil {
		t.Logger.Error(fmt.Errorf("service.GetTasksToBeReminded - : %w", err))
		return &task.TasksToBeReminded{}, status.Error(codes.Internal, "Couldn't get tasks to be reminded")
	}
	
	return res, nil
}
