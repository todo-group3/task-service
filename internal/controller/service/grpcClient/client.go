package grpcclient

import (
	"fmt"
	"time"

	"gitlab.com/todo-group/task-service/config"
	"gitlab.com/todo-group/task-service/genproto/task"
	"gitlab.com/todo-group/task-service/genproto/user"
	"gitlab.com/todo-group/task-service/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	Task() task.TaskServiceClient
	User() user.UserServiceClient
}

type ServiceManager struct {
	Config      *config.Config
	Log         *logger.Logger
	TaskService task.TaskServiceClient
	UserService user.UserServiceClient
}

func New(c *config.Config, log *logger.Logger) (Clients, error) {
	userConn, err := grpc.Dial(
		fmt.Sprintf("%s:%s", c.UserServiceHost, c.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		log.Fatal(fmt.Errorf("couldn't connect to user service: %w", err))
		return &ServiceManager{}, err
	}
	var taskConn *grpc.ClientConn
	for {
		taskConn, err = grpc.Dial(
			fmt.Sprintf("%s:%s", c.TaskServiceHost, c.TaskServicePort),
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		)
		if err == nil {
			break
		}
		time.Sleep(time.Second)
	}

	return &ServiceManager{
		Config:      c,
		Log:         log,
		TaskService: task.NewTaskServiceClient(taskConn),
		UserService: user.NewUserServiceClient(userConn),
	}, nil
}

func (s *ServiceManager) Task() task.TaskServiceClient {
	return s.TaskService
}

func (s *ServiceManager) User() user.UserServiceClient {
	return s.UserService
}
