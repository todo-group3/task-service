package storage

import (
	"gitlab.com/todo-group/task-service/internal/controller/storage/postgres"
	"gitlab.com/todo-group/task-service/internal/controller/storage/repo"
	"gitlab.com/todo-group/task-service/pkg/db"
)

type IStorage interface {
	Task() repo.TaskStorageI
}

type StoragePg struct {
	Db       *db.Postgres
	TaskRepo repo.TaskStorageI
}

// NewStoragePg
func NewStoragePg(pDb *db.Postgres) *StoragePg {
	return &StoragePg{
		Db:       pDb,
		TaskRepo: postgres.NewTaskRepo(pDb),
	}
}

func (s StoragePg) Task() repo.TaskStorageI {
	return s.TaskRepo
}
