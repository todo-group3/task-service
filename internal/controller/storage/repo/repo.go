package repo

import "gitlab.com/todo-group/task-service/genproto/task"

type TaskStorageI interface {
	ScheduleTask(req *task.ScheduleReq) (*task.ScheduleRes, error)
	UpdateTask(req *task.ScheduleReq) (*task.ScheduleRes, error)
	GetAllTasks(req *task.OneFieldReq) (*task.GetAllTasksRes, error)
	DoneTask(req *task.OneFieldReq) (*task.Empty, error)
	DeleteTask(req *task.OneFieldReq) (*task.Empty, error)
	IsDone(id string) (bool, error)
	GetTasksToBeReminded(req *task.GetTasksToBeRemindedReq) (*task.TasksToBeReminded, error)
}
