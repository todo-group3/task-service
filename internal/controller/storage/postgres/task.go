package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/todo-group/task-service/genproto/task"
)

func (r *TaskRepo) ScheduleTask(req *task.ScheduleReq) (*task.ScheduleRes, error) {
	var (
		startTime  time.Time
		finishTime time.Time
		createdAt  time.Time
		updatedAt  time.Time
		res        = &task.ScheduleRes{}
		err        error
	)

	startTime, err = time.Parse(time.RFC3339, req.StartTime)
	if err != nil {
		return &task.ScheduleRes{}, err
	}
	finishTime, err = time.Parse(time.RFC3339, req.FinishTime)
	if err != nil {
		return &task.ScheduleRes{}, err
	}
	query, _, err := r.Db.Builder.Insert("tasks").Columns(
		"id",
		"user_id",
		"title",
		"description",
		"start_time",
		"finish_time",
	).Values(
		req.Id,
		req.UserId,
		req.Title,
		req.Description,
		startTime,
		finishTime,
	).Suffix(
		`RETURNING 
			id,
			user_id, 
			title, 
			description,
			start_time,
			finish_time,
			created_at,
			updated_at
	`).ToSql()
	if err != nil {
		return &task.ScheduleRes{}, err
	}

	err = r.Db.Pool.QueryRow(context.Background(), query,
		req.Id,
		req.UserId,
		req.Title,
		req.Description,
		startTime,
		finishTime,
	).Scan(
		&res.Id,
		&res.UserId,
		&res.Title,
		&res.Description,
		&startTime,
		&finishTime,
		&createdAt,
		&updatedAt,
	)
	if err != nil {
		return &task.ScheduleRes{}, err
	}

	res.StartTime = fmt.Sprint(startTime.Format(time.RFC3339))
	res.FinishTime = fmt.Sprint(finishTime.Format(time.RFC3339))
	res.CreatedAt = fmt.Sprint(createdAt.Format(time.RFC3339))
	res.UpdatedAt = fmt.Sprint(updatedAt.Format(time.RFC3339))

	return res, nil
}

func (r *TaskRepo) UpdateTask(req *task.ScheduleReq) (*task.ScheduleRes, error) {
	var (
		startTime  time.Time
		finishTime time.Time
		createdAt  time.Time
		updatedAt  time.Time
		res        = &task.ScheduleRes{}
		err        error
	)

	startTime, err = time.Parse(time.RFC3339, req.StartTime)
	if err != nil {
		return &task.ScheduleRes{}, err
	}
	finishTime, err = time.Parse(time.RFC3339, req.FinishTime)
	if err != nil {
		return &task.ScheduleRes{}, err
	}

	query, _, err := r.Db.Builder.Update("tasks").Set(
		"title", req.Title,
	).Set(
		"description", req.Description,
	).Set(
		"start_time", startTime,
	).Set(
		"finish_time", finishTime,
	).Set(
		"updated_at", time.Now(),
	).Where("id=$6 and deleted_at is null and done_time is null").Suffix(
		`RETURNING 
		id,
		user_id, 
		title, 
		description, 
		start_time, 
		finish_time,
		created_at,
		updated_at 
	`).ToSql()
	if err != nil {
		return &task.ScheduleRes{}, err
	}

	err = r.Db.Pool.QueryRow(context.Background(), query,
		req.Title,
		req.Description,
		startTime,
		finishTime,
		time.Now(),
		req.Id).Scan(
		&res.Id,
		&res.UserId,
		&res.Title,
		&res.Description,
		&startTime,
		&finishTime,
		&createdAt,
		&updatedAt,
	)
	if err != nil {
		return &task.ScheduleRes{}, err
	}

	res.StartTime = fmt.Sprint(startTime.Format(time.RFC3339))
	res.FinishTime = fmt.Sprint(finishTime.Format(time.RFC3339))
	res.CreatedAt = fmt.Sprint(createdAt.Format(time.RFC3339))
	res.UpdatedAt = fmt.Sprint(updatedAt.Format(time.RFC3339))

	return res, nil
}

func (r *TaskRepo) DoneTask(req *task.OneFieldReq) (*task.Empty, error) {
	query, _, err := r.Db.Builder.Update("tasks").Set(
		"done_time", time.Now(),
	).Where("id=$2").ToSql()
	if err != nil {
		return &task.Empty{}, err
	}

	_, err = r.Db.Pool.Exec(context.Background(), query, time.Now(), req.Value)
	return &task.Empty{}, err
}

func (r *TaskRepo) IsDone(id string) (bool, error) {
	temp := 0
	err := r.Db.Pool.QueryRow(context.Background(), `select 1 from tasks 
	where id = $1 and done_time is not null and deleted_at is null`, id).Scan(
		&temp,
	)
	if err == sql.ErrNoRows {
		return false, nil
	}

	return temp == 1, nil
}

// reques is the id of user
func (r *TaskRepo) GetAllTasks(req *task.OneFieldReq) (*task.GetAllTasksRes, error) {
	var (
		startTime  time.Time
		finishTime time.Time
		createdAt  time.Time
		updatedAt  time.Time
		res        = &task.GetAllTasksRes{}
		err        error
	)
	query, _, err := r.Db.Builder.Select(
		"id",
		"user_id",
		"title",
		"description",
		"start_time",
		"finish_time",
		"created_at",
		"updated_at",
	).From("tasks").Where(
		"user_id = $1 and deleted_at is null", req.Value,
	).ToSql()
	if err != nil {
		return &task.GetAllTasksRes{}, err
	}

	rows, err := r.Db.Pool.Query(context.Background(), query, req.Value)
	if err != nil {
		return &task.GetAllTasksRes{}, err
	}
	defer rows.Close()

	for rows.Next() {
		temp := &task.GetTaskRes{}

		err := rows.Scan(
			&temp.Id,
			&temp.UserId,
			&temp.Title,
			&temp.Description,
			&startTime,
			&finishTime,
			&createdAt,
			&updatedAt,
		)
		if err != nil {
			return &task.GetAllTasksRes{}, err
		}
		temp.IsFinished, err = r.IsDone(temp.Id)
		if err != nil {
			return &task.GetAllTasksRes{}, err
		}
		temp.StartTime = fmt.Sprint(startTime.Format(time.RFC3339))
		temp.FinishTime = fmt.Sprint(finishTime.Format(time.RFC3339))
		temp.CreatedAt = fmt.Sprint(createdAt.Format(time.RFC3339))
		temp.UpdatedAt = fmt.Sprint(updatedAt.Format(time.RFC3339))

		res.Tasks = append(res.Tasks, temp)
		res.Count++
	}

	return res, nil
}

// Id of task is recieved
func (r *TaskRepo) DeleteTask(req *task.OneFieldReq) (*task.Empty, error) {
	_, err := r.Db.Pool.Exec(context.Background(), `UPDATE tasks SET deleted_at = NOW() where id = $1`, req.Value)

	return &task.Empty{}, err
}

func (r *TaskRepo) GetTasksToBeReminded(req *task.GetTasksToBeRemindedReq) (*task.TasksToBeReminded, error) {
	var (
		startTime  time.Time
		finishTime time.Time
		res        = &task.TasksToBeReminded{}
		err        error
		temp       = &task.TaskToBeReminded{}
	)

	whereQuery := "EXTRACT(EPOCH FROM (start_time - CURRENT_TIMESTAMP)) <= (300) and reminded_to_start is null and done_time is null"
	setField := "reminded_to_start"

	if req.StartOrFinish == "finish" {
		whereQuery = "CURRENT_TIMESTAMP >= finish_time and reminded_to_finish is null and done_time is null"
		setField = "reminded_to_finish"
	}
	query, _, err := r.Db.Builder.Update(
		"tasks",
	).Set(
		setField, time.Now(),
	).Where(
		whereQuery,
	).Suffix(
		`RETURNING 
			user_id, 
			title, 
			start_time,
			finish_time`,
	).ToSql()
	if err != nil {
		return &task.TasksToBeReminded{}, err
	}
	rows, err := r.Db.Pool.Query(context.Background(), query, time.Now())
	if err != nil {
		return &task.TasksToBeReminded{}, err
	}
	defer rows.Close()

	for rows.Next() {
		err := rows.Scan(
			&temp.UserId,
			&temp.Title,
			&startTime,
			&finishTime,
		)
		if err != nil {
			return &task.TasksToBeReminded{}, err
		}

		temp.StartTime = fmt.Sprint(startTime.Format(time.RFC3339))
		temp.FinishTime = fmt.Sprint(finishTime.Format(time.RFC3339))

		res.Tasks = append(res.Tasks, temp)
	}

	return res, nil
}
