package postgres

import (
	"gitlab.com/todo-group/task-service/pkg/db"
)

type TaskRepo struct {
	Db *db.Postgres
}

func NewTaskRepo(pDb *db.Postgres) *TaskRepo {
	return &TaskRepo{Db: pDb}
}
