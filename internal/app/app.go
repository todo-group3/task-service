// Package app configures and runs application.
package app

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/todo-group/task-service/config"
	ts "gitlab.com/todo-group/task-service/genproto/task"
	"gitlab.com/todo-group/task-service/internal/controller/service"
	grpcclient "gitlab.com/todo-group/task-service/internal/controller/service/grpcClient"
	"gitlab.com/todo-group/task-service/internal/rabbitmq"
	"gitlab.com/todo-group/task-service/pkg/db"
	"gitlab.com/todo-group/task-service/pkg/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Run creates objects via constructors.
func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)

	// Repository
	// postgres://user:password@host:5432/database
	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPoolMax))
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - postgres.New: %w", err))
	}
	defer pg.Close()

	clients, err := grpcclient.New(cfg, l)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	rmq := rabbitmq.New(cfg, l)

	taskService := service.NewTaskService(l, clients, pg, rmq)

	lis, err := net.Listen("tcp", ":"+cfg.TaskServicePort)
	if err != nil {
		l.Fatal(fmt.Errorf("app - Run - grpcclient.New: %w", err))
	}

	c := grpc.NewServer()
	reflection.Register(c)
	ts.RegisterTaskServiceServer(c, taskService)

	l.Info("Server is running on" + "port" + ": " + cfg.TaskServicePort)

	if err := c.Serve(lis); err != nil {
		log.Fatal("Error while listening: ", err)
	}
}
