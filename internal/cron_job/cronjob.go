package cronjob

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitlab.com/todo-group/task-service/config"
	"gitlab.com/todo-group/task-service/genproto/task"
	"gitlab.com/todo-group/task-service/genproto/user"
	grpcclient "gitlab.com/todo-group/task-service/internal/controller/service/grpcClient"
	"gitlab.com/todo-group/task-service/internal/controller/storage"
	"gitlab.com/todo-group/task-service/internal/rabbitmq"
	"gitlab.com/todo-group/task-service/pkg/db"
	"gitlab.com/todo-group/task-service/pkg/logger"
)

type CronJob struct {
	Log     *logger.Logger
	Cfg     *config.Config
	Rabbit  *rabbitmq.RabbitMQ
	Clients grpcclient.Clients
	Storage storage.IStorage
}

func New(log *logger.Logger, cfg *config.Config, rbt *rabbitmq.RabbitMQ, clns grpcclient.Clients, str storage.IStorage) *CronJob {
	return &CronJob{
		Log:     log,
		Cfg:     cfg,
		Rabbit:  rbt,
		Clients: clns,
		Storage: str,
	}
}

func Run(cfg *config.Config) {
	l := logger.New(cfg.LogLevel)
	clients, err := grpcclient.New(cfg, l)
	if err != nil {
		l.Fatal(fmt.Errorf("cronJob - Run - grpcclient.New: %w", err))
	}
	rmq := rabbitmq.New(cfg, l)

	pgxUrl := fmt.Sprintf("postgres://%s:%s@%s:%s/%s",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase)

	pg, err := db.New(pgxUrl, db.MaxPoolSize(cfg.PGXPoolMax))
	if err != nil {
		l.Fatal(fmt.Errorf("cron job - Run - postgres.New: %w", err))
	}
	defer pg.Close()
	stg := storage.NewStoragePg(pg)
	cronJob := New(l, cfg, rmq, clients, stg)

	l.Info("Cron Job is running")

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func() {
		cronJob.CheckStartReminds()
		wg.Done()
	}()

	go func() {
		cronJob.CheckFinishReminds()
		wg.Done()
	}()

	wg.Wait()
}

func (c *CronJob) CheckStartReminds() {
	var (
		tasks   = &task.TasksToBeReminded{}
		message = &task.RemainderReq{}

		err error
	)
	
	for {
		tasks, err = c.Storage.Task().GetTasksToBeReminded(&task.GetTasksToBeRemindedReq{
			StartOrFinish: "start",
		})
		if err != nil {
			time.Sleep(time.Minute)
			continue
		}

		for _, task := range tasks.Tasks {
			message.Start = task.StartTime
			message.End = task.FinishTime
			message.Title = task.Title
			message.Type = c.Cfg.RemainderType

			user, err := c.Clients.User().GetUserById(context.Background(), &user.GetUserReq{Value: task.UserId})
			if err != nil {
				c.Log.Error("User info not found")
				return
			}
			message.Email = user.Email
			message.FirstName = user.FirstName

			c.Rabbit.PublishRemindMessage(message)
		}
		time.Sleep(time.Minute)
	}
}

func (c *CronJob) CheckFinishReminds() {
	var (
		tasks   = &task.TasksToBeReminded{}
		message = &task.RemainderReq{}

		err error
	)
	for {
		tasks, err = c.Storage.Task().GetTasksToBeReminded(&task.GetTasksToBeRemindedReq{
			StartOrFinish: "finish",
		})
		if err != nil {
			fmt.Println(err)
			time.Sleep(time.Minute)
			continue
		}

		for _, task := range tasks.Tasks {
			message.Start = task.StartTime
			message.End = task.FinishTime
			message.Title = task.Title
			message.Type = c.Cfg.MissRemainderType

			user, err := c.Clients.User().GetUserById(context.Background(), &user.GetUserReq{Value: task.UserId})
			if err != nil {
				c.Log.Error("User info not found")
				return
			}
			message.Email = user.Email
			message.FirstName = user.FirstName

			c.Rabbit.PublishRemindMessage(message)
		}
		time.Sleep(time.Minute)
	}
}
