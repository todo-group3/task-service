package rabbitmq

import (
	"encoding/json"
	"fmt"

	"github.com/streadway/amqp"
	"gitlab.com/todo-group/task-service/config"
	"gitlab.com/todo-group/task-service/genproto/task"
	"gitlab.com/todo-group/task-service/pkg/logger"
	messagebroker "gitlab.com/todo-group/task-service/pkg/message_broker"
)

type RabbitMQ struct {
	Chan *amqp.Channel
	Log  *logger.Logger
	Cfg  *config.Config
}

func New(cfg *config.Config, log *logger.Logger) *RabbitMQ {
	conn, err := messagebroker.NewRabbitMQ(cfg)
	if err != nil {
		panic(err)
	}
	_, err = conn.Producer.QueueDeclare(
		cfg.RemainderTopic,
		false,
		true,
		false,
		false,
		nil,
	)
	if err != nil {
		log.Error(fmt.Errorf("rabbitmq.PublishRemindMessage - QueueDeclare: %w", err))
	}
	return &RabbitMQ{
		Chan: conn.Producer,
		Log:  log,
		Cfg:  cfg,
	}
}

func (r *RabbitMQ) PublishRemindMessage(req *task.RemainderReq) error {
	reqByte, err := json.Marshal(req)
	if err != nil {
		r.Log.Error(fmt.Errorf("rabbitmq.PublishRemindMessage : %w", err))
		return err
	}

	// _, err = r.Chan.QueueDeclare(
	// 	r.Cfg.RemainderTopic,
	// 	false,
	// 	true,
	// 	false,
	// 	false,
	// 	nil,
	// )
	// if err != nil {
	// 	r.Log.Error(fmt.Errorf("rabbitmq.PublishRemindMessage - QueueDeclare: %w", err))
	// 	return err
	// }

	err = r.Chan.Publish(
		"",
		"remind",
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body:        reqByte,
		},
	)

	if err != nil {
		r.Log.Error(fmt.Errorf("rabbitmq.PublishRemindMessage - Publish: %w", err))
	}
	return err
}
