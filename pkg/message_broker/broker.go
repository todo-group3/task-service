package messagebroker

import (
	"errors"
	"fmt"
	"time"

	"github.com/streadway/amqp"
	"gitlab.com/todo-group/task-service/config"
)

type RabbitMQ struct {
	Producer *amqp.Channel
}

func NewRabbitMQ(cfg *config.Config) (*RabbitMQ, error) {
	// connection string := amqp://user:password@host:port/
	conStr := fmt.Sprintf("amqp://%s:%s@%s:%s/",
		cfg.RabbitMQUser,
		cfg.RabbitMQPassword,
		cfg.RabbitMQHost,
		cfg.RabbitMQPort,
	)

	for cfg.RabbitMQConnectionTry > 0 {

		conn, err := amqp.Dial(conStr)
		if err == nil {
			ch, err := conn.Channel()
			if err != nil {
				return &RabbitMQ{}, err
			}
			return &RabbitMQ{
				Producer: ch,
			}, nil
		}
		time.Sleep(time.Second * time.Duration(cfg.RabbitMQConnectionTimeOut))
		cfg.RabbitMQConnectionTry--
	}

	return &RabbitMQ{}, errors.New("connection timeout")
}
